class CreateIssues < ActiveRecord::Migration[5.0]
  def change
    create_table :issues do |t|
      t.integer :number
      t.string :url

      t.timestamps
    end
    add_index :issues, :number
  end
end
