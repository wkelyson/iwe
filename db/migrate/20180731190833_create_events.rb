class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :action
      t.integer :issue_number

      t.timestamps
    end
    add_index :events, :issue_number
  end
end
