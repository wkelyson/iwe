Rails.application.routes.draw do
  root to: 'visitors#index'

  resources :events, only: [:create]

  resources :issues, only: [:show], defaults: { format: :json } do
    member do
      get :events
    end
  end
end
