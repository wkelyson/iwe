Improving Webhooks Events
================

This application allows you receives Github [Webooks](https://developer.github.com/webhooks/) events and stores it in Database.

Ruby on Rails
-------------

This application requires:

- Ruby 2.1.5
- Rails 5.0.7

Learn more about [Installing Ruby](https://www.ruby-lang.org/pt/documentation/installation/).
Learn more about [Installing Rails](http://railsapps.github.io/installing-rails.html).

Getting Started
---------------

1. Install the depencies above;
2. `$ cd improving-webooks-events` - Enter in the project folder
3. `$ bundle install` - Install the project dependencies
4. `$ cp config/database.yml.sample config/database.yml` - Copy the sample database file and change to your database settings
5. `$ bundle exec rake db:create db:migrate RAILS_ENV=test` - Create and setup the test database
6. `$ bundle exec rake rspec` - Run the specs to see everything is ok.
7. `$ bundle exec rails s -p 3000 -b 0.0.0.0` - Run the Rails server to see the project in action.

Credits
-------

- Wilbert Ribeiro (http://github.com/wilbert)

License
-------

MIT
