class IssuesController < ApplicationController
  respond_to :json

  def events
    @issue = Issue.find_by(number: params[:id])
    @events = @issue.events
    respond_with @events
  end
end
