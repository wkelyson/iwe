class EventsController < ApplicationController
  def create
    @issue = Issue.first_or_create(issue_params)
    @event = @issue.events.new(event_params)

    if @event.save
      render json: { status: :ok }
    else
      render json: { errors: @event.errors.full_messages }, status: 422
    end
  end

  private

  def issue_params
    params.require(:issue).permit(:number, :url)
  end

  def event_params
    params.require(:event).permit(:action)
  end
end
