class Issue < ApplicationRecord
  validates :number, presence: true

  has_many :events, foreign_key: :issue_number, primary_key: :number
end
