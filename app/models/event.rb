class Event < ApplicationRecord
  validates :issue_number, :action, presence: true

  has_one :issue, foreign_key: :issue_number
end
