FactoryBot.define do
  factory :issue do
    number 1
    url 'https://api.github.com/repos/wilbert/addresses/issues/6'
  end
end
