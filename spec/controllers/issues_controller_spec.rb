require 'rails_helper'

RSpec.describe IssuesController, type: :controller do
  let!(:issue) { create(:issue, number: 1000) }
  let!(:event_1) { create(:event, issue_number: issue.number, action: 'created') }
  let!(:event_2) { create(:event, issue_number: issue.number, action: 'delete') }
  let!(:event_3) { create(:event, issue_number: issue.number, action: 'push') }

  describe 'GET #events' do
    it 'should return all events of issue' do
      get :events, params: { id: issue.number, format: :json }
      expect(assigns(:issue)).to eq(issue)
      expect(assigns(:events)).to eq([event_1, event_2, event_3])
    end
  end
end
