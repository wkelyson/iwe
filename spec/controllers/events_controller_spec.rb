require 'rails_helper'

RSpec.describe EventsController, type: :controller do
  let!(:valid_attributes) {
    {
      issue: {
        number: 1000,
        url: 'https://api.github.com/repos/wilbert/addresses/issues/6'
      },
      event: {
        action: 'created'
      }
    }
  }

  let!(:invalid_attributes) {
    {
      issue: {
        number: 1000,
        url: 'https://api.github.com/repos/wilbert/addresses/issues/6'
      },
      event: {
        action: nil
      }
    }
  }

  describe 'POST #create' do
    context 'with valid attributes' do
      it 'should create a new Event' do
        expect {
          post :create, params: valid_attributes
        }.to change(Event, :count).by(1)
      end

      it 'should create a new Issue if it now exists' do
        expect {
          post :create, params: valid_attributes
        }.to change(Issue, :count).by(1)
      end

      it 'should not create a new Issue if it now exists' do
        create(:issue, number: 1000, url: 'https://api.github.com/repos/wilbert/addresses/issues/6')

        expect {
          post :create, params: valid_attributes
        }.to change(Issue, :count).by(0)
      end

      it 'render success' do
        post :create, params: valid_attributes
        expect(response).to be_success
      end
    end

    context 'with invalid attributes' do
      it 'should not create a new event' do
        expect {
          post :create, params: invalid_attributes
        }.to change(Event, :count).by(0)
      end

      it 'should not create a new event' do
        post :create, params: invalid_attributes

        expect(response.code).to eq('422')
      end
    end
  end
end
