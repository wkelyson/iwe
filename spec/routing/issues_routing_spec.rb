require 'rails_helper'

RSpec.describe "routes for Issues", type: :routing do
  it "routes /issues/1000/events to the issues controller" do
    expect(get("/issues/1000/events")).to route_to("issues#events", id: '1000', format: :json)
  end
end
