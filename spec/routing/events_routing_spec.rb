require 'rails_helper'

RSpec.describe "routes for Events", type: :routing do
  it "routes /events to the events controller" do
    expect(post("/events")).to route_to("events#create")
  end
end
